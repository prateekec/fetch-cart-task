let input = document.querySelector('header input');

input.addEventListener('keydown', searchItems);

function searchItems(event) {
    if (event.key == 'Enter') {
        let cntrs = document.querySelectorAll('.container');
        for (let cntr of cntrs) {
            cntr.style.display = "none";
        }
        let searchedItem = event.target.value;

        fetch('https://fakestoreapi.com/products')
            .then((res) => {
                return res.json();
            }).then((data) => {
                return fillSearchedResult(data, searchedItem);
            }).catch((err) => {
                console.log(err);
            });
    }
}

function fillSearchedResult(data, searchedItem) {
    // console.log(data);
    let newSection = document.createElement('section');
    newSection.classList.add('container');
    let divHeader = document.createElement('div');
    divHeader.innerHTML = "<h2>Searched Results...</h2>";
    divHeader.classList = "searched-header";
    let divSearchedArticles = document.createElement('div');
    divSearchedArticles.classList.add('searched-inside');
    divSearchedArticles.style.flexWrap = 'wrap';
    newSection.appendChild(divHeader);
    newSection.appendChild(divSearchedArticles);

    let body = document.querySelector('body');
    body.appendChild(newSection);

    for (let idx = 0; idx < data.length; idx++) {
        // console.log(data[idx]);
        let titleName = data[idx]["title"].toLowerCase();
        searchedItem = searchedItem.toLowerCase();
        // console.log(titleName);
        if (titleName.includes(searchedItem)) {
            console.log("yes");
            let newDiv = document.createElement('div');
            let img = document.createElement('img');
            let price = document.createElement('price');
            let title = document.createElement('p');
            newDiv.classList.add('single-article')
            newDiv.appendChild(img);
            newDiv.appendChild(title);
            newDiv.appendChild(price);
            img.src = data[idx]["image"];
            title.innerText = data[idx]['title'];
            price.innerHTML = "&dollar; " + data[idx++]['price'];
            divSearchedArticles.appendChild(newDiv);
        }

        // console.log(newDiv);

    }
    console.log(newSection);

}