fetch("https://fakestoreapi.com/products/category/electronics")
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    return fillElectronics(data);
  })
  .then(() => {
    return productViewer();
  })
  .catch((err) => {
    console.log(err);
  });

function fillElectronics(data) {
  let articles = document.querySelector(".electronics-inside");
  articles.style.flexWrap = "wrap";
  // console.log(articles)
  for (let idx = 0; idx < data.length; idx++) {
    // console.log(data[idx]);
    let newDiv = document.createElement("div");
    let img = document.createElement("img");
    let price = document.createElement("price");
    let title = document.createElement("p");
    newDiv.classList.add("single-article");
    newDiv.appendChild(img);
    newDiv.appendChild(title);
    newDiv.appendChild(price);
    img.src = data[idx]["image"];
    title.innerText = data[idx]["title"];
    price.innerHTML = "&dollar; " + data[idx++]["price"];
    newDiv.setAttribute("id", data[idx]["id"]);
    // console.log(newDiv);
    articles.appendChild(newDiv);
    let loader = document.querySelector(".loader-error");
    loader.classList.add("noDisplay");
    // console.log(newDisv);
  }
}
// console.log(document.querySelector('body'));
function productViewer() {
  let articles = document.querySelectorAll(".single-article");
  // console.log(articles);

  for (let article of articles) {
    article.addEventListener("click", showProduct);
  }

  function showProduct(event) {
    let article = event.target.parentElement;
    let id = article.getAttribute("id");
    let url = "https://fakestoreapi.com/products/" + id;
    fetch(url)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        return fillProduct(data);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function fillProduct(data) {
    console.log(data);
    if (document.querySelectorAll(".container")) {
      let cntrs = document.querySelectorAll(".container");
      for (let cntr of cntrs) {
        cntr.style.display = "none";
      }
    }
    let flex = document.querySelector(".show-product");
    flex.querySelector("img").src = data["image"];
    let info = flex.querySelector(".info");
    info.querySelector("h3").innerText = data["title"];
    info.querySelector(".rating .star p").innerHTML =
      data["rating"]["rate"] +
      ' <span style="font-size:100%;color:white;">&starf;</span> ';
    info.querySelector(".rating count").innerText =
      data["rating"]["count"] + " Ratings";
    info.querySelector(".price").innerHTML = "&dollar; " + data["price"];
    info.querySelector(".description p").innerText = data["description"];

    flex.style.display = "flex";

    // console.log(flex);
  }
}
