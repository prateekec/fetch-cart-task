fetch("https://fakestoreapi.com/products/category/electronics?limit=4")
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    return fillElectronics(data);
  })
  .catch((err) => {
    let nav = document.querySelector("nav");
    nav.classList.toggle("noDisplay");
    let cntrs = document.querySelectorAll(".container");

    for (let idx = 0; idx < cntrs.length; idx++) {
      cntrs[idx].style.display = "none";
      console.log(cntrs);
    }
    let showError = document.querySelector(".showError");
    showError.classList.toggle("noDisplay");
    let loader = document.querySelector(".loader");
    loader.classList.toggle("noDisplay");
    console.log(err, "sdlkfja;");
  });
fetch("https://fakestoreapi.com/products/category/jewelery?limit=4")
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    return fillJewelery(data);
  })
  .catch((err) => {
    console.log(err);
  });
fetch("https://fakestoreapi.com/products/category/men's clothing?limit=4")
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    return fillMens(data);
  })
  .catch((err) => {
    console.log(err);
  });
fetch("https://fakestoreapi.com/products/category/women's clothing?limit=4")
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    return fillWomens(data);
  })
  .catch((err) => {
    console.log(err);
  });

function fillElectronics(data) {
  // console.log(data);

  let articles = document.querySelectorAll(
    ".electronics-inside .single-article"
  );
  let idx = 0;
  for (let article of articles) {
    let articleImage = article.querySelector(".article-img");
    articleImage.querySelector(".loader").classList.toggle("loader");
    articleImage.querySelector("img").classList.toggle("noDisplay");
    let img = article.querySelector("img");
    img.src = data[idx]["image"];
    let para = article.querySelector("p");
    para.innerText = data[idx]["title"];
    let price = article.querySelector("price");
    price.innerHTML = "From " + "&dollar; " + data[idx]["price"];
    article.setAttribute("id", data[idx++]["id"]);
    let loader = document.querySelector(".loader-error");
    loader.classList.add("noDisplay");
    // console.log(article);
  }
}

function fillJewelery(data) {
  let articles = document.querySelectorAll(".jewelery-inside .single-article");
  let idx = 0;
  for (let article of articles) {
    let articleImage = article.querySelector(".article-img");
    articleImage.querySelector(".loader").classList.toggle("loader");
    articleImage.querySelector("img").classList.toggle("noDisplay");
    let img = article.querySelector("img");
    img.src = data[idx]["image"];
    let para = article.querySelector("p");
    para.innerText = data[idx]["title"];
    let price = article.querySelector("price");
    price.innerHTML = "From " + "&dollar; " + data[idx]["price"];
    article.setAttribute("id", data[idx++]["id"]);
  }
}

function fillMens(data) {
  let articles = document.querySelectorAll(
    ".mens-clothing-inside .single-article"
  );
  let idx = 0;
  for (let article of articles) {
    let articleImage = article.querySelector(".article-img");
    articleImage.querySelector(".loader").classList.toggle("loader");
    articleImage.querySelector("img").classList.toggle("noDisplay");
    let img = article.querySelector("img");
    img.src = data[idx]["image"];
    let para = article.querySelector("p");
    para.innerText = data[idx]["title"];
    let price = article.querySelector("price");
    price.innerHTML = "From " + "&dollar; " + data[idx]["price"];
    article.setAttribute("id", data[idx++]["id"]);
  }
}

function fillWomens(data) {
  let articles = document.querySelectorAll(
    ".womens-clothing-inside .single-article"
  );
  let idx = 0;
  for (let article of articles) {
    let articleImage = article.querySelector(".article-img");
    articleImage.querySelector(".loader").classList.toggle("loader");
    articleImage.querySelector("img").classList.toggle("noDisplay");
    let img = article.querySelector("img");
    img.src = data[idx]["image"];
    let para = article.querySelector("p");
    para.innerText = data[idx]["title"];
    let price = article.querySelector("price");
    price.innerHTML = "From " + "&dollar; " + data[idx]["price"];
    article.setAttribute("id", data[idx++]["id"]);
  }
}
