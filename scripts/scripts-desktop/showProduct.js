let articles = document.querySelectorAll('.single-article');
// console.log(articles);

for (let article of articles) {
    article.addEventListener('click', showProduct);
}

function showProduct(event) {
    let article = event.target.parentElement;
    if (article.classList.contains('article-img')) {
        article = article.parentElement;
    }
    let id = article.getAttribute('id');
    let url = "https://fakestoreapi.com/products/" + id;
    fetch(url)
        .then((res) => {
            return res.json();
        }).then((data) => {
            return fillProduct(data);
        }).catch((err) => {
            console.log(err);
        })
}

function fillProduct(data) {
    // console.log(data);

    let cntrs = document.querySelectorAll('.container');
    for (let cntr of cntrs) {
        cntr.style.display = "none";
    }
    let flex = document.querySelector('.show-product');
    flex.querySelector('img').src = data['image'];
    let info = flex.querySelector('.info');
    info.querySelector('h3').innerText = data["title"];
    info.querySelector('.rating .star p').innerHTML = data['rating']["rate"] + ' <span style="font-size:100%;color:white;">&starf;</span> ';
    info.querySelector('.rating count').innerText = data['rating']["count"] + " Ratings";
    info.querySelector('.price').innerHTML = "&dollar; " + data['price'];
    info.querySelector('.description p').innerText = data['description'];

    flex.style.display = "flex";

    // console.log(flex);

}