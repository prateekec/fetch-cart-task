let submitBtn = document.querySelector("#subBtn");
let signUpForm = document.querySelector("#signUpForm");

submitBtn.addEventListener("click", createAccount);

function createAccount(event) {
    event.preventDefault();

    let signUpForm = document.querySelector("#signUpForm");
    let firstName = signUpForm.querySelector("#firstName").value;
    let lastName = signUpForm.querySelector("#lastName").value;
    let email = signUpForm.querySelector("#email").value;
    let password = signUpForm.querySelector("#password").value;
    let rePassword = signUpForm.querySelector("#repeatPassword").value;
    let nameFlag = isEmpty(firstName, lastName);
    let emailFlag = validateEmail(email);
    let passwordFlag = validatePassword(password, rePassword);
    if (nameFlag && emailFlag && passwordFlag) {
        console.log("yes");
    } else {
        console.log("no");
    }
}
console.log(signUpForm);

function validateEmail(email) {
    let message = document.createElement("p");
    message.classList.add("email");

    let regex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    if (regex.test(email)) {
        if (signUpForm.querySelector(".email") != null) {
            signUpForm.removeChild(signUpForm.querySelector(".email"));
        }
        return true;
    } else {
        if (signUpForm.querySelector(".email") == null) {
            message.innerText = "*Email is wrong";
            signUpForm.prepend(message);
        }
        return false;
    }
}

function validatePassword(password, rePassword) {
    let message = document.createElement("p");
    message.classList.add("password");
    if (password == rePassword) {
        if (signUpForm.querySelector(".password") != null) {
            signUpForm.removeChild(signUpForm.querySelector(".password"));
        }
        return true;
    } else {
        if (signUpForm.querySelector(".password") == null) {
            message.innerText = "*Password is not same";
            signUpForm.prepend(message);
        }
        return false;
    }
}

function isEmpty(firstName, lastName) {
    let message = document.createElement("p");
    message.classList.add("name");
    if (firstName == "" || lastName == "") {
        if (signUpForm.querySelector(".name") == null) {
            message.innerText = "*One or more field is empty";
            signUpForm.prepend(message);
        }

        return false;
    } else {
        if (signUpForm.querySelector(".name") != null) {
            signUpForm.removeChild(signUpForm.querySelector(".name"));
        }
        return true;
    }
}
